function each(elements, callBack) {
    try{
        for(let index=0;index<elements.length;index++){
            callBack(elements[index], index, elements)
        }
    }

    catch(err){
        console.error(err);
    }
    
}

export default each;
