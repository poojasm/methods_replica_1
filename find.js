function find(elements, cb) {
  try{
    for (let index = 0; index < elements.length; index++) {
      const ifFound = cb(elements[index], index, elements);

      if (ifFound){
        return ifFound;
      }
    }
  }
  
  catch(err){
    console.error(err);
  }
}

export default find;
