function flatten(elements) {
  try{ 
    //given array- [1, [2], [[3]], [[[4]]]];
    const flatArray = [];

    function flattening(elements) {
      for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
          flattening(elements[index]);
          // if it's array at the index, then will call the function recursively..
        } 

        else {
          flatArray.push(elements[index]);
        }
      }
    }
  
    flattening(elements);
    return flatArray;
  }

  catch(err){
    console.error(err);
  }
}

export default flatten;
