// [1, 2, 3, 4].filter((num) => {
//   return num > 3;
// });

function filter(elements, cb) {
  try{
    const filteredArray = [];

    for (let index = 0; index < elements.length; index++) {
      const isTrue = cb(elements[index]);
    
      if (isTrue) {
        filteredArray.push(elements[index]);
      }
    }
    return filteredArray
  }

  catch(err){
    console.error(err);
  }
}

export default filter;
