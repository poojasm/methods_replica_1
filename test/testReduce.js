import items from "./input.js";
import reduce from "../reduce.js";

const callBack = function (accumilator, arrayItem) {
  accumilator = accumilator + arrayItem;
  return accumilator;
};

const result = reduce(items, callBack, 0);

console.log(result);
