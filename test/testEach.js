import items from "./input.js";
import each from "../each.js";

function callBack(item, idx, items) {
  console.log(`Item ${item} is at index ${idx} in the array ${items}`);
}
const result = each(items, callBack);

console.log(result);
