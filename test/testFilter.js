import items from "./input.js";
import filter from "../filter.js";

const callBack = function (item) {
  return item > 3;
};
const result = filter(items, callBack);

console.log(result);
