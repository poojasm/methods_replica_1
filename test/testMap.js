import items from "./input.js";
import map from "../map.js";

const callBack = function (item) {
  return item * 2;
};
const result = map(items, callBack);

console.log(result);
