import items from "./input.js";
import find from "../find.js";

const callBack = function (item) {
  if (item > 3) {
    return item;
  }
};
const result = find(items, callBack);

console.log(result);
