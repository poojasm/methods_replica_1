function map(elements, cb) {
  try{
      const resultArr = [];

      for (let index = 0; index < elements.length; index++) {
        const resultFromCallbackFunction = cb(elements[index]);
          resultArr.push(resultFromCallbackFunction);
      }

      return resultArr;
  }

  catch(err){
    console.error(err);
  }
}

export default map;
