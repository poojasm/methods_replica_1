function reduce(elements, cb, startingValue) {
  try{
    let sum = startingValue;

    for (let index = 0; index < elements.length; index++) {
      sum = cb(sum, elements[index], elements);
    }

    return sum;
  }

  catch(err){
    console.error(err);
  }
}

export default reduce;
